## Why this repo?
Sometimes you may forget certain CLI commands in Linux if you don't use them often.
This might happen when you don't use Linux CLI for your work or use a Gui based file manager in Linux instead.
The terminal in Linux is really powerful and fast, so to help remember certain commands I might need I made the list as a refresher.  

Inspired by Mental Outlaw on Youtube:
[source](https://www.youtube.com/watch?v=6KNI1Uj-Md8)

## Linux refresh: list of Linux terminal commands I should never ever ever forget
- **FIND command**: 

- this will find the file ".somedotfile" anywhere in your entire filesystem
```
sudo find / -name .somedotfile
```

if you don't need to look into directories that require root access, then use:
```diff
- sudo find / -name .somedotfile
+ find / -name .somedotfile
```

- Using wildcards to find any dotfiles in my home directory
 that start with a "."and end with "rc":
```
 find ~-name "."*rc"
```

To find a certain directory in your entire filesystem
for instance "Projects":
```
 sudo find / -type -d -name Projects
```
-type -d to search for a directory


To find a file that has the word 'project' but you don't remember if the filename started with a capital or not:
```
find . -iname '*project*' 
```
-iname means to ignore case  
. means to search in this (home) directory or subdirectories

Searches for files with file permissions: (for security audits in Linux)
```
find -type f -perm 0777 -print
```
find all files in home folder and its subdirectories with file permission 0777 and print these to the screen
0777 means you have all permissions: read write and execute for all users on your Linux system

Get list of all users on your Linux system
```
cut -d: -f1 /etc/passwd
```

File sizes:
Find all files that are greater than 1 Gigabyte
```
find . -type f -size +1G
```
Same but based on a range of filesizes:
```
find . -type f -size +1M -size -2G
``` 
